#include <cmath>
#include <cassert>
#include <iostream>

#include <TH1.h>
#include <TH2.h>

#include "Teddy.h"

using namespace std;

TVectorD identity (const TVectorD& x)
{
    TVectorD y(x);
    return y;
}

TVectorD Teddy::H2V (TH1 *h)
{
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tFrom histo to vector" << endl;
    TVectorD v(h->GetNbinsX());
    for(int i = 1; i <= h->GetNbinsX(); ++i)
        v(i-1) = h->GetBinContent(i);
    return v;
}

TMatrixD Teddy::H2M (TH2 *h)
{
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tFrom 2D histo to matrix" << endl;
    TMatrixD mat(h->GetNbinsY(), h->GetNbinsX());
    for(int j = 1; j <= h->GetNbinsX(); ++j)
    for(int i = 1; i <= h->GetNbinsY(); ++i)
        mat(i-1,j-1) = h->GetBinContent(j,i);
    return mat;
}

TMatrixD Teddy::TensorProduct (TVectorD v)
{
    const int n = v.GetNrows();
    TMatrixD m(n,n);
    for (int i=0; i<n; ++i)
    for (int j=0; j<n; ++j)
        m(i,j) = v(i)*v(j);
    return m;
}

TVectorD Teddy::sqrt (TVectorD In)
{
    const int nOldBins = In.GetNrows();
    TVectorD Out(nOldBins);
    for(int k = 0; k < nOldBins; ++k)
        Out(k) = ::sqrt(max(In(k),0.));
    return Out;
}

int Teddy::GuessOutDim (int nOldBins, function<TVectorD(const TVectorD &)> f)
{
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tGuessing dimension" << endl;
    TVectorD In(nOldBins);
    TVectorD Out = f(In);
    int d = Out.GetNrows();
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\td = " << d << endl;
    return d;
}


Teddy::Teddy (TH1 * inputDist, TH2 * inputCov,
        function<TVectorD(const TVectorD &)> f, int seed) :
    nOldBins(inputDist->GetNbinsX()),
    nNewBins(GuessOutDim(nOldBins, f)),
    N(0),
    // transform input (dist, cov) into (vector, matrix)
    vec(H2V(inputDist)),
    cov(H2M(inputCov)),
    // get transformation to new base where cov matrix is diagonal
    eigVals(nOldBins), // contains diagonalized errors (this line only defines the size of the vector)
    rotation(cov.EigenVectors(eigVals)), // diagonalise the cov matrix (the vector `eigVals` is modified)
    sigma(sqrt(eigVals)), //
    sum(nNewBins), // definition of the size of the output vector/distribution
    sum2(nNewBins, nNewBins), // definition of the size of the output covariance matrix/distribution
    func(f), // operation from input to output
    random(seed)
{
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tIn Teddy constructor" << endl;
    assert(nOldBins == inputCov->GetNbinsX());
    assert(nOldBins == inputCov->GetNbinsY());

    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tThe eigenvalues of the cov matrix are:\n";
    int nEigv = 0;
    for (int k = 0; k < nOldBins; ++k) {
        if (verbose)
            cout << eigVals(k) << '\n';
        if (eigVals(k) > 0)
            ++nEigv;
    }
    if (verbose) cout << endl;
    //rotation.Print();

    if (verbose) {
        cout << __FILE__ << ':' << __LINE__ << "\tSigma:\n";
        for (int k = 0; k < nOldBins; ++k) 
            cout << sigma(k) << '\n';
        cout << endl;
    }

    // sanity check
    int nFilledBins = 0;
    for (int i = 1; i <= nOldBins; ++i) {
        if (abs(inputDist->GetBinContent(i)) > 0)
            ++nFilledBins;
        else
            cout << i << ' ' << inputDist->GetBinContent(i) << endl;
    }

    assert(nFilledBins == nEigv);
}

void Teddy::play ()
{
    // 1) get vector in rotated basis
    TVectorD deltaP(nOldBins); // P for prime
    for (int k = 0; k < nOldBins; ++k) 
        deltaP(k) = random.Gaus(0, sigma(k));

    // 2) rotate the shift back
    TVectorD delta = rotation*deltaP;
    TVectorD x = vec + delta;

    // 3) compute whatever formula you like on x
    // dimension may change here!
    TVectorD y = func(x);
    sum+= y;

    // 4) get its uncertainty
    TMatrixD square = TensorProduct(y);
    sum2 += square;

    // 5) increment count for normalisation in Teddy::buy()
    ++N;
}

pair<TVectorD, TMatrixD> Teddy::buy () const
{
    TVectorD outputDist = sum*(1./N);

    TMatrixD avSq = sum2*(1./N);
    TMatrixD outputCov = avSq - TensorProduct(outputDist);

    return {outputDist, outputCov};
}

pair<TH1 *, TH2 *> Teddy::buy (TH1 * h, TH2 * cov) const
{
    assert(h != nullptr);
    assert(cov != nullptr);
    h->Reset();
    cov->Reset();

    TVectorD x(nNewBins);
    TMatrixD err(nNewBins, nNewBins);
    tie(x,err) = buy();

    for (int i=0; i<nNewBins; ++i) {
        double content = x(i),
               error = ::sqrt(err(i,i));

        h->SetBinContent(i+1, content);
        h->SetBinError  (i+1, error  );

        for (int j=0; j<nNewBins; ++j)
            cov->SetBinContent(i+1,j+1, err(i,j));
    }

    return {h,cov};
}

