#include <functional>
#include <algorithm>

#include <TVectorD.h>
#include <TMatrixD.h>

#include <TRandom3.h>

#include <TH2.h>

////////////////////////////////////////////////////////////////////////////////
/// identity (ROOT)
///
/// Trivial function returning argument, used as default function in the constructor of Teddy
TVectorD identity (const TVectorD& x);

////////////////////////////////////////////////////////////////////////////////
/// class Teddy
///
/// Use MC techniques to apply a function \f$f\f$ to a distribution $x$ with (non-diagonal) covariance matrix \f$\mathbf{V}\f$. 
/// 
/// Procedure:
///  1. Diagonalise the covariance matrix
///  2. Generate a multi-dimensional Gaussian event \f$\delta\f$ distributed according to the (sqrt of the) eigenvalues \f$\epsilon_i\f$
/// \f[
///     \delta_i \sim \mathcal{N}(0,\sqrt{\epsilon_i})
/// \f]
///  3. Transform back to the original base (where the cov matrix is non-diagonal)
///  4. Add rotated vector to the input distribution an apply the function \f$f\f$:
/// \f[
/// \mathbf{y}_k = 
/// \f]
///  5. Go back to 2. until a satisfying statistics is reached, with `Teddy::play()` (the criterion of satisfaction and the loop are to be defined outside of the `Teddy` instance)
///  6. Get output histogram and covariance matrix with `Teddy::buy()`
class Teddy {

public:
    static bool verbose;

private:
    static TVectorD H2V (TH1 *h); //!< conversion from (1D) histogram to vector
    static TMatrixD H2M (TH2 *h); //!< conversion from (2D) histogram to matrix

    static TMatrixD TensorProduct (TVectorD v); //!< outputs matrix \f$(v_i * v_j)\f$
    static TVectorD sqrt (TVectorD In); //!< takes the square root of each input vector

    ////////////////////////////////////////////////////////////////////////////////
    /// GuessOutDim
    ///
    /// guesses the size of the output distribution by calling once the function with some dummy input
    static int GuessOutDim (int nOldBins, std::function<TVectorD(const TVectorD &)> f);

    const int nOldBins, //!< number of bins of input distributions
              nNewBins; //!< number of bins of output distributions
    long long N; //!< number of calls

    const TVectorD vec; //!< distribution
    const TMatrixD cov; //!< covariance matrix

    TVectorD eigVals; //!< eigenvalues of input covariance matrix
    TMatrixD rotation; //!< rotation from diagonal basis to original basis

    const TVectorD sigma; //!< Gaussian widths in diagonal base

    TVectorD sum; //!< sum of events, increased at each call and from which output distribution is estimated
    TMatrixD sum2; //!< sum of tensor products of each event, increased at each call and from which output covariance is estimated

    std::function<TVectorD(const TVectorD &)> func; //!< transformation defining output as a function of the input

    TRandom3 random; //!< random generator

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Teddy (constructor)
    ///
    /// The rotation and the eigenvalues are determined from the input covariance matrix.
    /// Default function is the identity (useful for testing).
    Teddy
        (TH1 * inputDist, //!< input distribution
         TH2 * inputCov, //!< input covariance matrix
         std::function<TVectorD(const TVectorD &)> f = identity, //!< function to apply to the input distribution
         int seed = 42 //!< seed for random generator
         );

    void play (); //!< Needs to be called from a loop, but requires no argument
    std::pair<TVectorD, TMatrixD> buy () const; //!< Get output distributions, can be called several times.

    ////////////////////////////////////////////////////////////////////////////////
    /// buy
    ///
    /// Get output distributions, can be called several times.
    /// Input histrogams are reset and filled.
    /// The statistical uncertainty of the 1D histogram correspondings to the square-root of the diagonal elements of the covariance matrix 
    std::pair<TH1 *, TH2 *> buy 
        (TH1 * h, //!< histogram to be cloned for output distribution
         TH2 * cov //!< covariance matrix to be cloned for output distribution)
         ) const; 

    ////////////////////////////////////////////////////////////////////////////////
    /// buy
    ///
    /// Get output distributions, can be called several times.
    /// Input histrogams are defined from scratch and filled.
    /// The statistical uncertainty of the 1D histogram correspondings to the square-root of the diagonal elements of the covariance matrix 
    template<typename... Args> std::pair<TH1 *, TH2 *> buy (TString name, Args... args) const
    {
        TAxis * axis = new TAxis(args...);
        int ndiv = axis->GetNbins();
        double * edges = new double[ndiv];
        axis->GetLowEdge(edges);
        edges[ndiv-1] = axis->GetBinUpEdge(ndiv);
        TH1 * h   = new TH1D(name         , name, ndiv-1, edges);
        TH2 * cov = new TH2D(name + "_cov", name, ndiv-1, edges, ndiv-1, edges);

        return buy(h, cov);
    }
};
bool Teddy::verbose = true;

