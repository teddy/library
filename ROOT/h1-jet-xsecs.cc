#include "Teddy.h"

#include <cmath>

#include <tuple>
#include <algorithm>
#include <vector>
#include <limits>
#include <iostream>
#include <iomanip>
#include <fstream>

#include <TH1.h>
#include <TH2.h>
#include <TUnfoldBinning.h>
#include <TVectorD.h>
#include <TFile.h>

using namespace std;

// binning
static const vector<double> Pt1 { 4.5, 7.0, 11.0, 17.0, 25.0, 35.0, 50.0 },
                            Pt2 { 5.0, 7.0, 11.0, 17.0, 25.0, 35.0, 50.0 },
                            Pt3 { 5.0, 8.0, 12.0, 20.0, 40.0},
                            Q2 {5.5, 8, 11, 16, 22, 30, 42, 60, 80};
static const int nBinsP1 = Pt1.size()-1,
                 nBinsP2 = Pt2.size()-1,
                 nBinsP3 = Pt3.size()-1,
                 nBinsQ = Q2.size()-1;
static TUnfoldBinning * binning = new TUnfoldBinning("binning"),
                      * incl    = new TUnfoldBinning("InclJet"), // the names are used
                      * dijet   = new TUnfoldBinning("Dijet"  ), // to get the write
                      * trijet  = new TUnfoldBinning("Trijet" ); // input text files

template<typename Vector> Vector normaliseXsec (const Vector& x)
{
    Vector y(x);
    for (TUnfoldBinning * obs: {incl, dijet, trijet}) {
        double sum = 0.;
        for (int ibin = obs->GetStartBin(); ibin < obs->GetEndBin(); ++ibin)
            sum += x(ibin-1);
        bool normal = isnormal(sum);
        for (int ibin = obs->GetStartBin(); ibin < obs->GetEndBin(); ++ibin)
            y(ibin-1) = normal ? x(ibin-1)/sum : 0;
    }
    return y;
}

template<typename Vector> Vector normaliseQ2 (const Vector& x)
{
    Vector y(x);
    for (TUnfoldBinning * obs: {incl, dijet, trijet}) {
        const vector<double> * Pt = nullptr;
             if(obs == incl  ) Pt = &Pt1;
        else if(obs == dijet ) Pt = &Pt2;
        else if(obs == trijet) Pt = &Pt3;
        else                   assert(0);
        for (auto itQ2 = begin(Q2); itQ2 != prev(end(Q2)); ++itQ2) {
            double q2 = 0.5*(*itQ2 + *next(itQ2));
            double sum = 0.;
            for (auto itPt = begin(*Pt); itPt != prev(end(*Pt)); ++itPt) {
                double pt = 0.5*(*itPt + *next(itPt));
                int ibin = obs->GetGlobalBinNumber(pt,q2);
                sum += x(ibin-1);
            }
            bool normal = isnormal(sum);
            for (auto itPt = begin(*Pt); itPt != prev(end(*Pt)); ++itPt) {
                double pt = 0.5*(*itPt + *next(itPt));
                int ibin = obs->GetGlobalBinNumber(pt,q2);
                y(ibin-1) = normal ? x(ibin-1)/sum : 0;
            }
        }
    }
    return y;
}

void ReadCorrelations (TH2 * corrIn)
{
    ifstream f;
    f.open("DESY-16-200/d16-200.table-rhoij.abs.txt");
    assert(f.is_open());
    float content;
    for (int i = 1; i <= corrIn->GetNbinsX(); ++i) 
    for (int j = 1; j <= i                  ; ++j) {
        assert(f.good());
        f >> content;
        //cout << setw(3) << i << setw(5) << j << setw(15) << content << '\n';
        corrIn->SetBinContent(i,j,content);
        corrIn->SetBinContent(j,i,content);
        if (i == j) assert(         content  == 1);
        else        assert(std::abs(content) <= 1);
    }
    f.close();
}

TH2 * CalculateCorrelations (TH1 * h, TH2 * cov)
{
    TString hname = cov->GetName();
    hname.ReplaceAll("cov", "corr");
    auto corr = dynamic_cast<TH2*>(cov->Clone(hname));
    for (int i = 1; i <= h->GetNbinsX(); ++i)
    for (int j = 1; j <= i                ; ++j) {
        double sx = h->GetBinError(i),
               sy = h->GetBinError(j),
               c = cov->GetBinContent(i,j),
               cxy = round(c*100 / (sx*sy));
        corr->SetBinContent(i,j,cxy);
        corr->SetBinContent(j,i,cxy);
    }
    corr->GetZaxis()->SetTitle("[%]");
    corr->SetMinimum(-100);
    corr->SetMaximum( 100);
    return corr;
}

void ReadDistribution (TH1 * hIn, TUnfoldBinning * obs)
{
    ifstream f;
    auto fname = Form("DESY-16-200/d16-200.table-cs.h%s.txt", obs->GetName()); 
    cout << fname << '\n';
    f.open(fname);
    assert(f.is_open());
    float lowQ2, highQ2, lowPt, highPt, sigma, relStat;
    while (true) {
        f >> lowQ2 >> highQ2 >> lowPt >> highPt >> sigma >> relStat;
        if (f.eof() || f.fail()) break;
        float pt = 0.5*(lowPt+highPt),
              q2 = 0.5*(lowQ2+highQ2),
              err = sigma * relStat / 100;
        int i = obs->GetGlobalBinNumber(pt,q2);
        cout << setw(3) << i << setw(10) << pt << setw(10) << q2 << setw(15) << sigma << setw(15) << err << '\n';
        hIn->SetBinContent(i,sigma);
        hIn->SetBinError  (i,err  );
    }
    cout << flush;
    f.close();
}

int main (int argc, char * argv[])
{
    const int N = argc > 1 ? atoi(argv[1]) : 1e5;

    incl  ->AddAxis("Pt1", nBinsP1, Pt1.data(),false,false);
    dijet ->AddAxis("Pt2", nBinsP2, Pt2.data(),false,false);
    trijet->AddAxis("Pt3", nBinsP3, Pt3.data(),false,false);
    incl  ->AddAxis("Q2", nBinsQ, Q2.data(),false,false);
    dijet ->AddAxis("Q2", nBinsQ, Q2.data(),false,false);
    trijet->AddAxis("Q2", nBinsQ, Q2.data(),false,false);

    binning->AddBinning(incl);
    binning->AddBinning(dijet);
    binning->AddBinning(trijet);

    TH1 * hIn    = binning->CreateHistogram           ("hIn"   , false, 0, ";bin ID; #sigma  [pb/GeV^{3}]");
    TH2 * covIn  = binning->CreateErrorMatrixHistogram("covIn" , false, 0, ";bin ID;bin ID;");
    TH2 * corrIn = binning->CreateErrorMatrixHistogram("corrIn", false, 0, ";bin ID;bin ID;[%]");

    for (TUnfoldBinning * obs: {incl, dijet, trijet})
        ReadDistribution(hIn, obs);
    ReadCorrelations(corrIn);

    cout << "Calculating covariance" << endl;
    for (int i = 1; i <= hIn->GetNbinsX(); ++i) 
    for (int j = 1; j <= i; ++j) {
        auto x = hIn->GetBinError(i),
             y = hIn->GetBinError(j),
             corr = corrIn->GetBinContent(i,j),
             content = x*y * corr;
        covIn->SetBinContent(i,j,content);
        covIn->SetBinContent(j,i,content);
    }

    cout << "Rescaling correlations in percents" << endl;
    corrIn->Scale(100);
    corrIn->SetMinimum(-100);
    corrIn->SetMaximum( 100);

    cout << "Creating output file" << endl;
    auto f = TFile::Open(Form("%s.root", argv[0]), "RECREATE");
    hIn->SetDirectory(f);
    covIn->SetDirectory(f);
    corrIn->SetDirectory(f);

    // preparing output for normalisation to cross sections
    cout << "Preparing output histograms" << endl;
    TH1 * hXsecOut    = binning->CreateHistogram           ("hXsecOut"   , false, 0, ";bin ID;normalised cross sections");
    TH2 * covXsecOut  = binning->CreateErrorMatrixHistogram("covXsecOut" , false, 0, ";bin ID;bin ID;");

    // preparing output for normalisation per Q2 bins
    TH1 * hQ2Out    = binning->CreateHistogram           ("hQ2Out"   , false, 0, ";bin ID;normalised cross sections");
    TH2 * covQ2Out  = binning->CreateErrorMatrixHistogram("covQ2Out" , false, 0, ";bin ID;bin ID;");

    // operation 
    cout << "Initialising Teddy" << endl;
    Teddy::verbose = false;
    Teddy toyXsec(hIn, covIn, normaliseXsec<
#ifdef TOY_EIGEN
            Eigen::VectorXd
#else
            TVectorD
#endif
            >),
          toyQ2(hIn, covIn, normaliseQ2<
#ifdef TOY_EIGEN
            Eigen::VectorXd
#else
            TVectorD
#endif
            >);
    // note that by default, the same seed is used for both

    cout << "Looping:" << endl;
    int percent = 0;
    cout << setw(3) << percent << flush;
    for (int i = 0; i < N; ++i) {
        if ((100*i) % N == 0) {
            cout << '\r' << setw(3) << percent << '%' << flush;
            ++percent;
        }
        toyXsec.play();
        toyQ2.play();
    }
    cout << "\r100%" << endl;

    cout << "Retrieving output" << endl;
    tie(hXsecOut, covXsecOut) = toyXsec.buy(hXsecOut, covXsecOut);
    tie(hQ2Out  , covQ2Out  ) = toyQ2  .buy(hQ2Out  , covQ2Out  );

    cout << "Calculating correlation matrices" << endl;
    TH2 * corrXsecOut = CalculateCorrelations(hXsecOut, covXsecOut);
    TH2 * corrQ2Out   = CalculateCorrelations(hQ2Out  , covQ2Out  );

    // saving output
    cout << "Writin to file" << endl;
    for (TH1 * h: {hIn, hXsecOut, hQ2Out}) h->Write();
    for (TH2 * h: {covIn, covXsecOut, covQ2Out, corrIn, corrXsecOut, corrQ2Out}) h->Write();
    f->Close();

    cout << "End" << endl;
    return 0;
}
