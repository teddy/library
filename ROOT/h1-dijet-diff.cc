#include "Teddy.h"

#include <cmath>

#include <tuple>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <limits>

#include <TUnfoldBinning.h>
#include <TVectorD.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

using namespace std;

// https://arxiv.org/abs/1412.0928
// https://www-h1.desy.de/psfiles/papers/desy14-200.pdf
// https://www-h1.desy.de/h1/www/publications/htmlsplit/DESY-14-200.long.poster.html

// directly taken from table 5 in latex file
static const vector<pair<double,double>> xsec {
    {3.35,9.1},
    {1.84,7.1},
    {0.834,7.2},
    {0.344,8.6},
    {0.0613,11.7},

    {1.23,15.1},
    {0.578,12.9},
    {0.287,12.6},
    {0.100,17.6},
    {0.0276,17.4},

    {0.122,26.6},
    {0.0511,24.7},
    {0.0207,30.0},
    {0.0160,20.1},
    {0.0034,27.8}
};

// directly taken from table 9 in latex file
static const vector<vector<double>> corr {
    {100, -7,  1,  0,  0,  -44,  2,  0,  0,  0,   13,  1,  1,  1,  0},
    {    100, -3,  0,  0,    3,-57,  3, -1,  0,    0, 17,  1,  1,  1},
    {        100, -2,  1,    1,  3,-59,  1, -1,    2,  1, 22,  1,  1},
    {            100,  3,   -1,  0,  0,-58,  1,    2,  1,  2, 25,  2},
    {                100,    0,  0,  0,  1,-56,    0,  0,  1,  2, 27},

    {                      100, -7,  3,  3,  1,  -60,  2, -5, -6, -3},
    {                          100, -4,  2,  1,    3,-57,  0, -3, -2},
    {                              100,  0,  2,   -6,  0,-60, -4, -4},
    {                                  100,  1,   -7, -3, -6,-62, -3},
    {                                      100,   -4, -2, -5, -5,-64},

    {                                            100, -5, 13, 14,  7},
    {                                                100,  0,  6,  3},
    {                                                    100, 10,  9},
    {                                                        100,  8},
    {                                                            100}
};

// binning
static const vector<double> pt1 {5.5, 7, 9, 15},
                            Q2 {4,6,10,18,34,100};
static const int nBinsP = pt1.size()-1,
                 nBinsQ = Q2.size()-1;

template<typename Vector> Vector closure (const Vector& x)
{
    return Vector(x);
}

template<typename Vector> Vector normalise (const Vector& x)
{
    Vector y(x);
    for (int iQ2 = 0; iQ2 < nBinsQ; ++iQ2) {
        double sum = 0.;
        for (int ipt1 = 0; ipt1 < nBinsP; ++ipt1) 
            sum += x(ipt1*nBinsQ+iQ2);
        bool normal = isnormal(sum);
        for (int ipt1 = 0; ipt1 < nBinsP; ++ipt1) 
            y(ipt1*nBinsQ+iQ2) = normal ? x(ipt1*nBinsQ+iQ2)/sum : 0;
    }
    return y;
}

void Print (TH1 * h)
{
    for (int i = 1; i <= h->GetNbinsX(); ++i)
        cout << setw(7) << h->GetBinContent(i) << '\t';
    cout << '\n' << endl;
}

void Print (TH2 * h)
{
    for (int i = 1; i <= h->GetNbinsX(); ++i) {
        for (int j = 1; j <= h->GetNbinsY(); ++j)
            cout << setw(5) << static_cast<int>(round(h->GetBinContent(i, j))) << '\t';
        cout << '\n';
    }
    cout << endl;
}

int main (int argc, char * argv[])
{
    const int N = argc > 1 ? atoi(argv[1]) : 1e7;

    assert(nBinsP*nBinsQ == 15);
    assert(xsec.size() == 15);
    assert(corr.size() == 15);

    TUnfoldBinning * binning = new TUnfoldBinning("binning");
    binning->AddAxis("pt1", nBinsP, pt1.data(), false, false);
    binning->AddAxis("Q2" , nBinsQ, Q2 .data(), false, false);

    auto f = TFile::Open(Form("%s.root", argv[0]), "RECREATE");

    // preparing input
    TH1 * hIn    = binning->CreateHistogram           ("hIn"   , false, 0, ";bin ID (p*_{T,1}, Q^{2}); #frac{d#sigma}{dp*_{T,1}dQ^{2}}  [pb/GeV^{3}]");
    TH2 * covIn  = binning->CreateErrorMatrixHistogram("covIn" , false, 0, ";bin ID (p*_{T,1}, Q^{2});bin ID (p*_{T,1}, Q^{2});");
    TH2 * corrIn = binning->CreateErrorMatrixHistogram("corrIn", false, 0, ";bin ID (p*_{T,1}, Q^{2});bin ID (p*_{T,1}, Q^{2});[%]");

    // getting histogram from 1D vector
    for (int i = 1; i <= 15; ++i) {
        auto content =           xsec.at(i-1).first       ,
             error   = content * xsec.at(i-1).second / 100;
        hIn->SetBinContent(i,content);
        hIn->SetBinError  (i,error  );
    }

    // getting correlations from 2D vector
    for (int i = 1; i <= 15; ++i)
    for (int j = i; j <= 15; ++j) {
        auto c = corr.at(i-1).at(j-i);
        corrIn->SetBinContent(i,j,c);
        corrIn->SetBinContent(j,i,c);
    }
    corrIn->SetMinimum(-100);
    corrIn->SetMaximum( 100);

    // getting covariance
    for (int i = 1; i <= 15; ++i) 
    for (int j = 1; j <= 15; ++j) {
        auto x = hIn->GetBinError(i),
             y = hIn->GetBinError(j),
             corr = corrIn->GetBinContent(i,j),
             content = x*y * corr/100;
        covIn->SetBinContent(i,j,content);
    }

    cout << "\nInput\n" << endl;
    Print(hIn);
    Print(corrIn);

    // preparing output
    TH1 * hOut    = binning->CreateHistogram           ("hOut"   , false, 0, ";bin ID (p*_{T,1}, Q^{2}); #frac{1}{#frac{d#sigma}{dQ^{2}}} #frac{d^{2}#sigma}{dp*_{T,1}dQ^{2}}  [1/GeV]");
    TH2 * covOut  = binning->CreateErrorMatrixHistogram("covOut" , false, 0, ";bin ID (p*_{T,1}, Q^{2});bin ID (p*_{T,1}, Q^{2});");
    TH2 * corrOut = binning->CreateErrorMatrixHistogram("corrOut", false, 0, ";bin ID (p*_{T,1}, Q^{2});bin ID (p*_{T,1}, Q^{2});[%]");

    // operation 
    Teddy::verbose = false;
    Teddy toy(hIn, covIn, normalise<
#ifdef TOY_EIGEN
            Eigen::VectorXd
#else
            TVectorD
#endif
            >);
    for (int i = 0; i < N; ++i)
        toy.play();
    tie(hOut, covOut) = toy.buy(hOut, covOut);

    // calculating correlations
    for (int i = 1; i <= 15; ++i)
    for (int j = 1; j <= 15; ++j) {
        double sx = hOut->GetBinError(i),
               sy = hOut->GetBinError(j),
               c = covOut->GetBinContent(i,j),
               cxy = round(c*100 / (sx*sy));
        corrOut->SetBinContent(i,j,cxy);
    }
    corrOut->SetMinimum(-100);
    corrOut->SetMaximum( 100);

    cout << "\nOutput\n" << endl;
    Print(hOut);
    Print(corrOut);

    // saving output
    TH2 * hIn2 = (TH2 *) binning->ExtractHistogram("hIn2", hIn);
    TH2 * hOut2 = (TH2 *) binning->ExtractHistogram("hOut2", hOut);
    for (TH1 * h: {hIn, hOut}) h->Write();
    for (TH2 * h: {hIn2, hOut2, covIn, covOut, corrIn, corrOut}) h->Write();
    f->Close();

    return 0;
}
