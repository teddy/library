#include "Teddy.h"

#include <iostream>
#include <tuple>

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

using namespace std;

const vector<double> pt_edges {18,21,24,28,32,37,43,49,56,64,74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846};
const int nPtBins = pt_edges.size() - 1;

template<typename Vector> Vector func (const Vector& x)
{
    Vector fraction(nPtBins);
    for (int i = 0; i < nPtBins; ++i) {
        auto b = x(nPtBins+i),
             incl = b+x(i);
        if (incl > 0) fraction(i) = b/incl;
    }
    return fraction;
}

TH2 * cov2corr (TH2 * cov, const char * name)
{
    auto corr = dynamic_cast<TH2*>(cov->Clone(name));
    for (int x = 1; x <= corr->GetNbinsX(); ++x) 
    for (int y = 1; y <= corr->GetNbinsY(); ++y) {
        auto sx = sqrt(cov->GetBinContent(x,x)),
             sy = sqrt(cov->GetBinContent(y,y)),
             c = cov->GetBinContent(x,y);
        if (sx && sy > 0) {
            c /= sx * sy;
            c *= 100;
            corr->SetBinContent(x,y,c);
        }
    }
    return corr;
}

int main (int argc, char * argv[])
{
    const int N = argc > 1 ? atoi(argv[1]) : 1e6;

    auto f = TFile::Open("HF.root", "READ");
    auto hIn = dynamic_cast<TH1*>(f->Get("h"));
    auto covIn = dynamic_cast<TH2*>(f->Get("cov"));
    hIn->SetDirectory(0);
    covIn->SetDirectory(0);
    f->Close();

    // extract correlations with physical axis
    auto corrIn    = cov2corr(covIn   , "correlations");
    TH2 * inclCorr  = new TH2D("inclCorr" , ";Jet p_{T}   [GeV];Jet p_{T}    [GeV];correlations [%]", nPtBins, pt_edges.data(), nPtBins, pt_edges.data()),
        * bCorr     = new TH2D("bCorr"    , ";Jet p_{T}   [GeV];Jet p_{T}    [GeV];correlations [%]", nPtBins, pt_edges.data(), nPtBins, pt_edges.data()),
        * crossCorr = new TH2D("crossCorr", ";Jet p_{T}   [GeV];Jet p_{T}    [GeV];correlations [%]", nPtBins, pt_edges.data(), nPtBins, pt_edges.data());
    for (int i = 1; i <= nPtBins; ++i) 
    for (int j = 1; j <= nPtBins; ++j) {
        auto i2 = i + nPtBins, j2 = j + nPtBins;
        auto sIncl  = corrIn->GetBinContent(i ,j ),
             sB     = corrIn->GetBinContent(i2,j2),
             sCross = corrIn->GetBinContent(i ,j2);
        bCorr    ->SetBinContent(i,j,               sB);
        crossCorr->SetBinContent(i,j,        sCross   );
        inclCorr ->SetBinContent(i,j,sIncl+2*sCross+sB);
    }

    // extract cross sections & naive fraction
    TH1 * inclXsec  = new TH1D("inclXsec" , ";Jet p_{T}   [GeV];#frac{d#sigma_{incl}}{dp_{T}}   [pb/GeV]"                  , nPtBins, pt_edges.data()),
        * bXsec     = new TH1D("bXsec"    , ";Jet p_{T}   [GeV];#frac{d#sigma_{HF}}{dp_{T}}   [pb/GeV]"                    , nPtBins, pt_edges.data()),
        * naiveFrac = new TH1D("naiveFrac", ";Jet p_{T}   [GeV];#frac{d#sigma_{HF}}{dp_{T}}/#frac{d{#sigma_{incl}}{dp_{T}}", nPtBins, pt_edges.data());
    for (int i = 1; i <= nPtBins; ++i) {
        auto j = i+nPtBins;
        auto b    =     hIn->GetBinContent(j),
             incl = b + hIn->GetBinContent(i),
             bSig2    =         covIn->GetBinContent(j,j),
             inclSig2 = bSig2 + covIn->GetBinContent(i,i);
        bXsec->SetBinContent(i,      b     );
        bXsec->SetBinError  (i, sqrt(bSig2));
        inclXsec->SetBinContent(i,      incl     );
        inclXsec->SetBinError  (i, sqrt(inclSig2));
        if (incl > 0) {
            naiveFrac->SetBinContent(i, b/incl);
            //naiveFrac->SetBinError  (i, /* TODO */);
        }
    }

#ifdef TOY_EIGEN
    Teddy toy(hIn, covIn, func<Eigen::VectorXd>);
#else
    Teddy toy(hIn, covIn, func<TVectorD>);
#endif
    for (int i = 0; i < N; ++i)
        toy.play();

    f = TFile::Open("fraction.root", "RECREATE");
    TH1 * hOut   = new TH1D("frac"          , ";Jet p_{T}   [GeV];#frac{d#sigma_{HF}}{dp_{T}}/#frac{d{#sigma_{incl}}{dp_{T}}", nPtBins, pt_edges.data());
    TH2 * covOut = new TH2D("covarianceFrac", ";Jet p_{T}   [GeV];Jet p_{T}   [GeV];", nPtBins, pt_edges.data(), nPtBins, pt_edges.data());
    tie(hOut, covOut) = toy.buy(hOut, covOut);
    auto corrOut = cov2corr(covOut, "correlationsFrac");

    for (TH2 * h: { inclCorr, bCorr, crossCorr, corrOut}) {
        h->SetMinimum(-100);
        h->SetMaximum( 100);
    }

    for (TH1 * h: { inclXsec, bXsec, naiveFrac, hOut}) h->Write();
    for (TH2 * h: { inclCorr, bCorr, crossCorr, covOut, corrOut}) h->Write();

    f->Close();

    return 0;
}
