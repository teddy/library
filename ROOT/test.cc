#include "Teddy.h"

#include <iostream>
#include <tuple>

#include <TVectorD.h>
#include <TH1.h>
#include <TH2.h>

using namespace std;

#ifdef TOY_EIGEN
using namespace Eigen;
VectorXd func (const VectorXd& x)
{
    VectorXd y(2);
    y(0) = x(0) + x(1);
    y(1) = x(2);
    return y;
}
#else
TVectorD func (const TVectorD& x)
{
    TVectorD y(2);
    y(0) = x(0) + x(1);
    y(1) = x(2);
    return y;
}

#endif

void Print (TH1 * h)
{
    for (int i = 1; i <= h->GetNbinsX(); ++i)
        cout << h->GetBinContent(i) << '\t';
    cout << '\n' << endl;
}

void Print (TH2 * h)
{
    for (int i = 1; i <= h->GetNbinsX(); ++i) {
        for (int j = 1; j <= h->GetNbinsY(); ++j)
            cout << h->GetBinContent(i, j) << '\t';
        cout << '\n';
    }
    cout << endl;
}

int main (int argc, char * argv[])
{
    const int N = argc > 1 ? atoi(argv[1]) : 1e6;

    const int nBinsIn = 3;
    TH1 * hIn = new TH1D("hIn", "hIn", nBinsIn, 0, nBinsIn);
    TH2 * covIn = new TH2D("covIn",  "covIn", nBinsIn, 0, nBinsIn, nBinsIn, 0, nBinsIn);

    cout << "\nInput\n" << endl;

    hIn->SetBinContent(1, 1);
    hIn->SetBinContent(2, 2);
    hIn->SetBinContent(3, 1);
    Print(hIn);

    covIn->SetBinContent(1, 1, 1);
    covIn->SetBinContent(1, 2, 1);
    covIn->SetBinContent(1, 3, 0);
    covIn->SetBinContent(2, 1, 1);
    covIn->SetBinContent(2, 2, 4);
    covIn->SetBinContent(2, 3, 0);
    covIn->SetBinContent(3, 1, 0);
    covIn->SetBinContent(3, 2, 0);
    covIn->SetBinContent(3, 3, 1);
    Print(covIn);

    cout << "---------" << endl;

    const int nBinsOut = 2;
    TH1 * hOut = new TH1D("hOut", "hOut", nBinsOut, 0, nBinsOut);
    TH2 * covOut = new TH2D("covOut",  "covOut", nBinsOut, 0, nBinsOut, nBinsOut, 0, nBinsOut);

    {
        Teddy toy(hIn, covIn, func);
        for (int i = 0; i < N; ++i)
            toy.play();

        tie(hOut, covOut) = toy.buy(hOut, covOut);

        cout << "\nOutput\n" << endl;

        Print(hOut);
        Print(covOut);
    }

    return 0;
}
