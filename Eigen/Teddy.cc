#include <cmath>
#include <cassert>
#include <iostream>

#include <TH1.h>
#include <TH2.h>

#include "Teddy.h"

#include "eigen/Eigen/Eigenvalues"

using namespace std;
using namespace Eigen;

VectorXd identity (const VectorXd& x)
{
    VectorXd y(x);
    return y;
}

VectorXd Teddy::H2V (TH1 *h)
{
    if (verbose) cout << __FILE__ << ':' << __LINE__ << "\tFrom histo to vector" << endl;
    VectorXd v(h->GetNbinsX());
    for(int i = 1; i <= h->GetNbinsX(); ++i)
        v(i-1) = h->GetBinContent(i);
    return v;
}

MatrixXd Teddy::H2M (TH2 *h)
{
    if (verbose) cout << __FILE__ << ':' << __LINE__ << "\tFrom 2D histo to matrix" << endl;
    int siz = h->GetNbinsX();
    assert(siz == h->GetNbinsY());
    MatrixXd mat(siz,siz);
    for(int j = 1; j <= siz; ++j)
    for(int i = 1; i <= siz; ++i)
        mat(i-1,j-1) = h->GetBinContent(j,i);
    return mat;
}

int Teddy::GuessOutDim (int nOldBins, function<VectorXd(const VectorXd &)> f)
{
    if (verbose) cout << __FILE__ << ':' << __LINE__ << "\tGuessing dimension" << endl;
    VectorXd In(nOldBins);
    VectorXd Out = f(In); // guessing the size from the output of the function
    return Out.size();
}


Teddy::Teddy (TH1 * inputDist, TH2 * inputCov,
        function<VectorXd(const VectorXd &)> f, int seed) :
    // counters
    nOldBins(inputDist->GetNbinsX()),
    nNewBins(GuessOutDim(nOldBins, f)),
    N(0),
    // type
    vec(H2V(inputDist)),
    cov(H2M(inputCov)),
    // rotation
    eigVals(nOldBins),
    rotation(nOldBins,nOldBins),
    sigma(nOldBins),
    // result
    sum(nNewBins), // definition of the size of the output vector/distribution
    sum2(nNewBins, nNewBins), // definition of the size of the output covariance matrix/distribution
    // operations
    func(f),
    random(seed)
{
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tIn Teddy constructor" << endl;
    assert(nOldBins == inputCov->GetNbinsX());
    assert(nOldBins == inputCov->GetNbinsY());

    // initialising `sum` and `sum2`
    for (int i = 0; i < nNewBins; ++i) {
        sum(i) = 0;
        for (int j = 0; j < nNewBins; ++j) 
            sum2(i,j) = 0;
    }

    assert(cov == cov.transpose().eval()); // check that the matrix is symmetric
    SelfAdjointEigenSolver<MatrixXd> es(cov);
    eigVals = es.eigenvalues();
    rotation = es.eigenvectors();
    if (verbose) cout << __FILE__ << ':' << __LINE__ << "\tThe eigenvalues of the cov matrix are:\n" << eigVals << endl;
    //if (verbose) cout << "The rotation matrix is:\n" << rotation << endl;

    // the eigenvalues are ordered by increasing value, therefore it is sufficient to check only the first value
    assert(eigVals(0) >= 0);
    sigma = eigVals.cwiseSqrt();
    if (verbose) cout << __FILE__ << ':' << __LINE__ << "\tSigma:\n" << sigma << '\n';

    int nEigv = 0;
    for (int k = 0; k < nOldBins; ++k)
        if (eigVals(k) > 0)
            ++nEigv;
    if (verbose) cout << endl;

    // sanity check
    int nFilledBins = 0;
    for (int i = 1; i <= nOldBins; ++i) {
        if (abs(inputDist->GetBinContent(i)) > 0)
            ++nFilledBins;
        else
            cout << __FILE__ << ':' << __LINE__ << '\t' << i << ' ' << inputDist->GetBinContent(i) << endl;
    }

    assert(nFilledBins == nEigv);
}

void Teddy::play ()
{
    // 1) get vector in rotated basis
    VectorXd deltaP(nOldBins); // P for prime
    for (int k = 0; k < nOldBins; ++k) 
        deltaP(k) = random.Gaus(0, sigma(k));

    // 2) rotate the shift back
    VectorXd delta = rotation*deltaP;
    VectorXd x = vec + delta;

    // 3) compute whatever formula you like on x
    // dimension may change here!
    VectorXd y = func(x);
    sum+= y;

    // 4) get its uncertainty
    MatrixXd square = y * y.transpose(); // tensor product
    sum2 += square;

    // 5) increment count for normalisation in Teddy::buy()
    ++N;
}

pair<VectorXd, MatrixXd> Teddy::buy () const
{
    VectorXd outputDist = sum*(1./N);

    MatrixXd avSq = sum2*(1./N);
    MatrixXd outputCov = avSq - outputDist * outputDist.transpose();

    return {outputDist, outputCov};
}

pair<TH1 *, TH2 *> Teddy::buy (TH1 * h, TH2 * cov) const
{
    assert(h != nullptr);
    assert(cov != nullptr);
    h->Reset();
    cov->Reset();

    VectorXd x(nNewBins);
    MatrixXd err(nNewBins, nNewBins);
    tie(x,err) = buy();

    for (int i=0; i<nNewBins; ++i) {
        double content = x(i),
               error = sqrt(err(i,i));

        h->SetBinContent(i+1, content);
        h->SetBinError  (i+1, error  );

        for (int j=0; j<nNewBins; ++j)
            cov->SetBinContent(i+1,j+1, err(i,j));
    }

    return {h,cov};
}
