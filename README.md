# Teddy: Treat Entries out of the Diagonal DecentlY

Tested successfully with g++ 9.3.0, ROOT6/24, and Eigen 3.4.90.

## Installation

We assume g++ and ROOT to be already available on your system.
Eigen can be downloaded automatically as a submodule.

To compute, go in either subdirectory and use `make`.
To see the test, just run `./test`.

## Measurements

### Jet and dijet cross sections

#### Measurement of Dijet Production in Diffractive Deep-Inelastic ep Scattering at HERA 

 - [DESY-14-200](https://www-h1.desy.de/h1/www/publications/htmlsplit/DESY-14-200.long.poster.html)
 - [arxiv:1412.0928](https://arxiv.org/abs/1412.0928)
 - [Inspire](https://inspirehep.net/literature/1332186)
	
#### Measurement of Jet Production Cross Sections in Deep-inelastic ep Scattering at HERA

 - [DESY-16-200](https://www-h1.desy.de/h1/www/publications/htmlsplit/DESY-16-200.long.poster.html)
 - [arxiv:1412.0928](https://arxiv.org/abs/1611.03421)
 - [Inspire](https://inspirehep.net/literature/1496981)
 - [HEP record](https://www.hepdata.net/record/ins1496981)

### Jet substructure

#### Properties of jet fragmentation using charged particles measured with the ATLAS detector in pp collisions at sqrt(s) = 13 TeV

 - [article](https://journals.aps.org/prd/pdf/10.1103/PhysRevD.100.052011)
 - [HEP record]()

####

 - [article](Study of quark and gluon jet substructure in Z+jet and dijet events from pp collisions)
 - [HEP record](https://www.hepdata.net/record/ins1920187)

